from django.urls import include, path
from .routers import *
from .views import LogoutView, MyObtainTokenPairView

app_name = 'main'
urlpatterns = [
    path('', include(router.urls)),
    path('logout/', LogoutView.as_view(), name='auth_logout'),
    path('token/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),

]
