from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'profile', views.ProfileViewSet)
router.register(r'purchase', views.PurchaseViewSet)
router.register(r'review', views.ReviewViewSet)
router.register(r'gig', views.GigViewSet)