from rest_framework import permissions
from .models import Gig
from django.contrib.auth.models import User



class IsOwnerProfile(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            user_profile = User.objects.get(
                pk=view.kwargs['pk'])
        except:
            return False

        if request.user.profile == user_profile:
            return True

        return False


class IsNotAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        return not request.user.is_authenticated