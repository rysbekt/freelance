from django.contrib.auth.models import User
from rest_framework import permissions, status, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView

from .mixins import ReadWriteSerializerMixin
from .models import Gig, Purchase, Review
from .permissions import IsOwnerProfile, IsNotAuthenticated
from .serializers import GigWriteSerializer, GigReadSerializer, RegisterSerializer, PurchaseSerializer, ReviewSerializer
from .serializers import MyTokenObtainPairSerializer


class MyObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = MyTokenObtainPairSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = (permissions.IsAuthenticated(), IsOwnerProfile())

        if self.request.method == 'POST':
            self.permission_classes = [IsNotAuthenticated()]

        if self.request.method == 'GET':
            self.permission_classes = [permissions.AllowAny()]
        return self.permission_classes


class LogoutView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class GigViewSet(ReadWriteSerializerMixin, viewsets.ModelViewSet):
    queryset = Gig.objects.filter(status="WA")
    read_serializer_class = GigReadSerializer
    write_serializer_class = GigWriteSerializer
    permission_classes = [permissions.IsAuthenticated()]

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = [permissions.IsAuthenticated(), IsOwnerProfile()]

        if self.request.method == 'POST':
            self.permission_classes = [permissions.IsAuthenticated()]

        if self.request.method == 'GET':
            self.permission_classes = [permissions.AllowAny()]

        return self.permission_classes


class PurchaseViewSet(viewsets.ModelViewSet):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = [permissions.IsAuthenticated()]

        if self.request.method == 'POST':
            self.permission_classes = [permissions.IsAuthenticated()]

        if self.request.method == 'GET':
            self.permission_classes = [permissions.IsAuthenticated()]

        return self.permission_classes


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = [permissions.IsAuthenticated(), IsOwnerProfile()]

        if self.request.method == 'POST':
            self.permission_classes = [permissions.IsAuthenticated()]

        if self.request.method == 'GET':
            self.permission_classes = [permissions.AllowAny()]

        return self.permission_classes
