from django.contrib import admin
from .models import  Gig, Purchase, Review

# Register your models here.
admin.site.register(Gig)
admin.site.register(Purchase)
admin.site.register(Review)
