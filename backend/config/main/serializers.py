from .models import Gig, Review, Purchase
from django.contrib.auth.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers


# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'first_name', 'last_name', 'password'
        )


# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data);
        return user


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)
        # Add custom claims
        token['user'] = {'id': user.id, 'username': user.username, 'email': user.email, 'first_name': user.first_name,
                         'last_name': user.last_name}
        return token


class GigWriteSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = Gig
        exclude = ["create_time", "status"]

    def get_user(self, obj):
        return obj.user.id

    def create(self, validated_data):
        validated_data['user'] = User.objects.get(id=get_user_id(self))
        gig = Gig.objects.create(**validated_data)
        gig.status = "WA"
        gig.save()
        return gig


class GigReadSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    purchases = serializers.SerializerMethodField()

    class Meta:
        model = Gig
        fields = "__all__"

    def get_user(self, obj):
        return obj.user.id

    def get_purchases(self, obj):
        queryset = Purchase.objects.filter(gig_id=obj.id).values()
        return queryset


class PurchaseSerializer(serializers.ModelSerializer):
    buyer = serializers.SerializerMethodField()

    class Meta:
        model = Purchase
        exclude = ["time"]

    def get_buyer(self, obj):
        return obj.buyer.id

    def get_gig(self, obj):
        return obj.gig.id

    def create(self, validated_data):
        request = self.context.get('request', None)
        validated_data['buyer'] = User.objects.get(id=request.user.id)
        gig = validated_data['gig']
        gig.status = "WA"
        gig.save()
        try:
            purchase = Purchase.objects.get(buyer=validated_data['buyer'], gig=validated_data['gig'])
        except:
            purchase = Purchase.objects.create(**validated_data)
        return purchase

    def update(self, pk, validated_data):
        if validated_data["status"] == "WO":
            gig = validated_data["gig"]
            if get_user_id(self) == gig.user.id:
                gig.status = "WO"
                gig.save()

                purchases = Purchase.objects.filter(gig=gig)
                for x in purchases:
                    x.status = "CL"
                    x.save()
                instance = super().update(pk, validated_data)
            else:
                instance = Purchase.objects.get(pk=pk)

        elif validated_data["status"] == "CL":
            gig = validated_data["gig"]
            gig.status = "CL"
            gig.save()
            instance = super().update(pk, validated_data)
        return instance


class ReviewSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = "__all__"

    def get_user(self, obj):
        return obj.user.username

    def get_gig(self, obj):
        return obj.gig.id

    def create(self, validated_data):
        validated_data['user'] = User.objects.get(id=get_user_id(self))
        review = Review.objects.create(**validated_data)
        return review


def get_user_id(self):
    request = self.context.get('request', None)
    if request:
        return request.user.id
