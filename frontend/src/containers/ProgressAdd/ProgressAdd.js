import React, {useEffect} from 'react';
import Grid from "@material-ui/core/Grid";
import {useDispatch, useSelector} from "react-redux";
import {fetchProgress} from "../../store/actions/addsActions";
import Typography from "@material-ui/core/Typography/Typography";
import Add from "../Adds/Add";


const ProgressAdd = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const adds = useSelector(state => state.adds.adds);
  const progress = useSelector(state => state.adds.progress);
  useEffect(() => {
    dispatch(fetchProgress());
  }, [dispatch]);

  const myAdds = progress.filter(elem => elem.buyer === user.id);

  const add = myAdds.map(elem => {
    return adds.find(e => e.id === elem.gig)
  });

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>Adds in Progress</Typography>
        </Grid>
      </Grid>
      {add.length>0? <Grid item container spacing={1}>
          {add.map(add => (
            <Add
              key={add.id}
              id={add.id}
              title={add.title}
              price={add.price}
              image={add.photo}
            />
          ))}
        </Grid>:
        <Grid item container spacing={1}>
          Ooops, no tasks taken, just take one
        </Grid>}
    </Grid>
  )
};

export default ProgressAdd;