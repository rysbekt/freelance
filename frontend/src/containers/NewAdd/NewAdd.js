import React from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import AddForm from "../../components/AddForm/AddForm";
import {createAdd} from "../../store/actions/addsActions";

const NewAdd = () => {

  const dispatch = useDispatch();
  const categories = [
    {_id: 'GD', title: 'Graphics&Design'},
    {_id: 'DM', title: 'Digital&Marketing'},
    {_id: 'VA', title: 'Video&Animation'},
    {_id: 'MA', title: 'Music&Audio'},
    {_id: 'PT', title: 'Programming&Tech'}
  ];


  const onProductsFormSubmit = async productData => {
    await dispatch(createAdd(productData));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          New job posting
        </Typography>
      </Grid>
      <Grid item xs>
        <AddForm onSubmit={onProductsFormSubmit} categories={categories}/>
      </Grid>
    </Grid>
  );
};

export default NewAdd;