import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import {useParams} from "react-router-dom";
import {CardMedia, MenuItem} from "@material-ui/core/index";
import imageNotAvailable from "../../assets/images/notAvailable.png";
import {apiURL} from "../../config";
import {fetchAdds, takeTask} from "../../store/actions/addsActions";
import Typography from "@material-ui/core/Typography/Typography";
import TextField from "@material-ui/core/TextField";
import FormElement from "../../components/UI/Form/FormElement";
import Button from "@material-ui/core/Button";


const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  },
  card: {
    height: '50%'
  },
  media: {
    paddingTop: '60%',
  },
  content: {
    marginBottom: '10px'
  }
}));

const SingleAdd = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const adds = useSelector(state => state.adds.adds);
  const categories = useSelector(state => state.categories.categories);
  const status = useSelector(state => state.categories.status);

  const {id} = useParams();

  const add = adds.find(elem => elem.id.toString() === id.toString());
  const category = categories.find(elem => elem._id.toString() === add.category.toString());
  const stats = status.find(elem => elem._id.toString() === add.status.toString());

  let cardImage = imageNotAvailable;
  if (add.photo) {
    cardImage = apiURL + '/' + add.photo;
  }

  const [state, setState] = useState({
    status: '',
    gig: '',
  });

  useEffect(() => {
    dispatch(fetchAdds());
    setState(prevState => ({
      ...prevState, gig: add.id
    }));

  }, [dispatch, add.id]);

  const error = useSelector(state => state.adds.addsError);

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(takeTask(state));
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }


  return (
    <Grid>
      <Card className={classes.card}>
        <CardHeader title={add.title}/>
        <CardContent>
          <Grid className={classes.content}>
            <Typography variant='body2'>{new Date(add.create_time).toLocaleDateString()}</Typography>
          </Grid>
          <Grid item className={classes.content} xs={12} sm={8} md={4} lg={4}>
            <CardMedia
              image={cardImage}
              title={add.title}
              className={classes.media}
            />
          </Grid>
          <Grid className={classes.content}>
            <Typography variant='body2'>
              <strong> Category: </strong>{category.title}
            </Typography>
          </Grid>
          <Grid className={classes.content}>
            <Typography variant='body2'>
              <strong> Price: </strong>{add.price} KGS
            </Typography>
          </Grid>
          <Grid className={classes.content}>
            <Typography variant='body2'>
              <strong> Description: </strong>{add.description}
            </Typography>
          </Grid>
          {add.purchases.length > 0 ?
            <Grid className={classes.content}>
              <strong>Status history: </strong>
              {add.purchases.map(elem => (
                <Typography key={elem.id} variant='body2'>
                  {elem.status} at {new Date(elem.time).toLocaleDateString()}

                </Typography>
              ))}
            </Grid> :
            <Grid className={classes.content}>
              <Typography variant='body2'>
                <strong> Status: </strong>{stats.title}
              </Typography>
            </Grid>
          }
        </CardContent>
        {user ?
          <Grid container direction="column">
            <Grid item xs>
              <form onSubmit={submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                  <Grid item xs>
                    <TextField
                      required
                      select
                      variant="outlined"
                      label="Status"
                      name="status"
                      value={state.status}
                      onChange={inputChangeHandler}
                    >
                      <MenuItem><i>Select a category</i></MenuItem>
                      {status.map(status => (
                        <MenuItem key={status._id} value={status._id}>
                          {status.title}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <FormElement label={'task'}
                               onChange={inputChangeHandler}
                               name={'task'}
                               disabled
                               value={add.id}
                               type={'text'}
                               error={getFieldError('title')}
                  />
                  <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">
                      Do id!
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </Grid>
          </Grid> : <div></div>
        }

      </Card>
    </Grid>
  )
};

export default SingleAdd;