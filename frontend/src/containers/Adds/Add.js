import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";

import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {CardMedia} from "@material-ui/core";

import imageNotAvailable from '../../assets/images/notAvailable.png';
import {historyPush} from "../../store/actions/historyActions";
import {useDispatch} from "react-redux";

const useStyles = makeStyles({
  card: {
    height: '100%'
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  }
});


const Add = ({title, price, image, id}) => {
  const classes = useStyles();
  const dispatch = useDispatch();


  let cardImage = imageNotAvailable;
  if (image) {
    cardImage = image;
  }

  return (
    <Grid item xs={12} sm={12} md={6} lg={4}>
      <Card className={classes.card}>
        <CardHeader title={title}/>
        <CardMedia
          image={cardImage}
          title={title}
          className={classes.media}
        />
        <CardContent>
          <strong style={{marginLeft: '10px'}}>
            Price: {price} KGS
          </strong>
        </CardContent>
        <CardActions>
          <IconButton onClick={()=>dispatch(historyPush('/add/' + id))} component={Link} to={'/add/' + id}>
            <ArrowForwardIcon/>
          </IconButton>
        </CardActions>
      </Card>
    </Grid>
  );
};

Add.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string
};

export default Add;