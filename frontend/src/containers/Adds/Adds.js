import React from 'react';
import {useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Add from "./Add";

const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  }
}));

const Adds = ({adds, category}) => {
  const classes = useStyles();
  const loading = useSelector(state => state.adds.addsLoading);

  let categoryText = 'All categories';
  if (category) {
    categoryText = category;
  }


  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>{categoryText}</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={1}>
        {loading ? (
          <Grid container justify='center' alignContent="center" className={classes.progress}>
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>) : adds.map(add => (
          <Add
            key={add.id}
            id={add.id}
            title={add.title}
            price={add.price}
            image={add.photo}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default Adds;