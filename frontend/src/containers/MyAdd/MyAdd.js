import React from 'react';
import Grid from "@material-ui/core/Grid";
import {useSelector} from "react-redux";
import Typography from "@material-ui/core/Typography/Typography";
import Add from "../Adds/Add";


const MyAdd = () => {
  const user = useSelector(state => state.users.user);
  const adds = useSelector(state => state.adds.adds);


  const myAdds = adds.filter(add => add.user === user.id);


  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant='h4'>My adds</Typography>
        </Grid>
      </Grid>
      {myAdds.length>0? <Grid item container spacing={1}>
        {myAdds.map(add => (
          <Add
            key={add.id}
            id={add.id}
            title={add.title}
            price={add.price}
            image={add.photo}
          />
        ))}
      </Grid>:
        <Grid item container spacing={1}>
          Ooops, no ads posted, just post one
        </Grid>}

    </Grid>
  )
};

export default MyAdd;