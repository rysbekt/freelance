import axiosApi from "../../axiosApi";
import {NotificationManager} from "react-notifications";
import {historyPush} from "./historyActions";

export const FETCH_ADDS_REQUEST = 'FETCH_ADDS_REQUEST';
export const FETCH_ADDS_SUCCESS = 'FETCH_ADDS_SUCCESS';
export const FETCH_ADDS_FAILURE = 'FETCH_ADDS_FAILURE';

export const FETCH_ADD_SUCCESS = 'FETCH_ADD_SUCCESS';

export const CHANGE_ADD_SUCCESS = 'CHANGE_ADD_SUCCESS';

export const CLEAR_ERRORS_ADDS = 'CLEAR_ERRORS_ADDS';

export const fetchAddsRequest = () => ({type: FETCH_ADDS_REQUEST});
export const fetchAddsSuccess = adds => ({type: FETCH_ADDS_SUCCESS, adds});
export const fetchAddsFailure = error => ({type: FETCH_ADDS_FAILURE, error});

export const fetchAddSuccess = add => ({type: FETCH_ADD_SUCCESS, add});

export const changeAddSuccess = add => ({type: CHANGE_ADD_SUCCESS, add});

export const clearErrorsAdds = () => ({type: CLEAR_ERRORS_ADDS});

export const fetchAdds = () => {
  return async dispatch => {
    try {
      dispatch(fetchAddsRequest());
      const response = await axiosApi.get('/api/gig/');
      dispatch(fetchAddsSuccess(response.data));

    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(fetchAddsFailure(e.response.data));
      } else {
        dispatch(fetchAddsFailure({global: 'No internet'}));
        NotificationManager.error('Could not fetch adds')
      }
    }
  };
};

export const fetchProgress = () => {
  return async dispatch => {
    try {
      dispatch(fetchAddsRequest());
      const response = await axiosApi.get('api/purchase/');
      dispatch(fetchAddSuccess(response.data));
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(fetchAddsFailure(e.response.data));
      } else {
        dispatch(fetchAddsFailure({global: 'No internet'}));
        NotificationManager.error('Could not fetch adds')
      }
    }
  };
};

export const fetchSingleAdd = id => {
  return async dispatch => {
    try {
      dispatch(fetchAddsRequest());
      const response = await axiosApi.get('/adds/' + id);
      dispatch(fetchAddSuccess(response.data));
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(fetchAddsFailure(e.response.data));
      } else {
        dispatch(fetchAddsFailure({global: 'No internet'}));
        NotificationManager.error('Could not fetch adds')
      }
    }
  };
};

export const createAdd = (add) => {
  return async dispatch => {
    try {
      dispatch(fetchAddsRequest());
      const response = await axiosApi.post('/api/gig/', add);
      dispatch(changeAddSuccess(response.data));
      dispatch(historyPush('/'));
      NotificationManager.success('Add was posted');
    } catch (e) {
      console.log(e)
      if (e.response && e.response.data) {
        dispatch(fetchAddsFailure(e.response.data));
      } else {
        dispatch(fetchAddsFailure({global: 'No internet'}));
      }
    }
  };
};

export const takeTask = (add) => {
  return async dispatch => {
    try {
      dispatch(fetchAddsRequest());
      await axiosApi.post('/api/purchase/', add);
      dispatch(changeAddSuccess());
      NotificationManager.success('Add was taken');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(fetchAddsFailure(e.response.data));
      } else {
        dispatch(fetchAddsFailure({global: 'No internet'}));
      }
    }
  };
};

export const deleteAdd = (id, token) => {
  return async dispatch => {
    try {
      dispatch(fetchAddsRequest());
      await axiosApi.delete('/adds/' + id, {headers: {'Authorization': token}});
      dispatch(fetchAdds());
      dispatch(changeAddSuccess({}));
      dispatch(historyPush('/'));
      NotificationManager.success('Add was deleted');
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(fetchAddsFailure(e.response.data));
        NotificationManager.error(e.response.data.error);
      } else {
        dispatch(fetchAddsFailure({global: 'No internet'}));
      }
    }
  }
};