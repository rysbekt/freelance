import axios from '../../axiosApi';
import {historyPush} from "./historyActions";
import jwt_decode from "jwt-decode";
import {NotificationManager} from "react-notifications";

export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

export const CLEAR_ERRORS = 'CLEAR_ERRORS';

const registerUserRequest = () => ({type: REGISTER_USER_REQUEST});
const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});


const loginUserRequest = () => ({type: LOGIN_USER_REQUEST});
const loginUserSuccess = token => ({type: LOGIN_USER_SUCCESS, token});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const clearErrors = () => ({type: CLEAR_ERRORS});


export const logoutUser = () => {
  return async (dispatch, useSelector) => {;
    dispatch({type: LOGOUT_USER});
    dispatch(historyPush('/'));
    NotificationManager.success('Logged out successfully');
  }
};

export const registerUser = userData => {
  return async dispatch => {
    try {
      dispatch(registerUserRequest());
      const response = await axios.post('/api/profile/', userData);
      dispatch(registerUserSuccess(response.data));
      dispatch(historyPush('/user/login'));
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(registerUserFailure(error.response.data));
      } else {
        dispatch(registerUserFailure({global: 'No internet'}));
      }
    }
  };
};

export const loginUser = userData => {
  return async dispatch => {
    try {
      dispatch(loginUserRequest());
      const response = await axios.post('/api/token/', userData);
      const decoded = jwt_decode(response.data.access);
      dispatch(registerUserSuccess(decoded.user));
      dispatch(loginUserSuccess(response.data));
      dispatch(historyPush('/'));
      NotificationManager.success('Signed in successfully')
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(loginUserFailure(error.response.data));
      } else {
        dispatch(loginUserFailure({global: 'No internet'}));
      }
    }
  };
};