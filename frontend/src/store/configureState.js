import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import usersReducer, {initialState} from "./reducers/usersReducer";
import categoriesReducer from "./reducers/categoriesReducer";
import addsReducer, {initialStateAdd} from "./reducers/addsReducer";
import axiosApi from "../axiosApi";


const rootReducer = combineReducers({
  users: usersReducer,
  categories: categoriesReducer,
  adds: addsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      ...initialState,
      user: store.getState().users.user,
      token: store.getState().users.token,
    },
    adds: {
      ...initialStateAdd,
      adds: store.getState().adds.adds,
    }
  })
});


axiosApi.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = `Bearer ${store.getState().users.token.access}`;
  } catch (e) {
    //no token exists
  }
  return config;
});

axiosApi.interceptors.response.use(res => res, e => {
  if (!e.response) {
    e.response = {data: {global: 'No internet'}}
  }
  throw e
});


export default store;