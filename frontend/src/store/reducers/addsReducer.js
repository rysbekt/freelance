import {
  CHANGE_ADD_SUCCESS,
  CLEAR_ERRORS_ADDS,
  FETCH_ADD_SUCCESS,
  FETCH_ADDS_FAILURE,
  FETCH_ADDS_REQUEST,
  FETCH_ADDS_SUCCESS
} from "../actions/addsActions";


export const initialStateAdd = {
  adds: [],
  progress: [],
  addsLoading: false,
  addsError: null
};

const addsReducer = (state = initialStateAdd, action) => {
  switch (action.type) {
    case CLEAR_ERRORS_ADDS:
      return {...state, registerError: null, addsError: null};
    case FETCH_ADDS_REQUEST:
      return {...state, addsLoading: true};
    case FETCH_ADDS_SUCCESS:
      return {...state, adds: action.adds, addsLoading: false};
    case FETCH_ADD_SUCCESS:
      return {...state, progress: action.add, addsLoading: false};
    case CHANGE_ADD_SUCCESS:
      return {...state, addsLoading: false, add: action.add};
    case FETCH_ADDS_FAILURE:
      return {...state, addsLoading: false, addsError: action.error};
    default:
      return state;
  }
};

export default addsReducer;