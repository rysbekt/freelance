import {FETCH_CATEGORIES_SUCCESS} from "../actions/categoryActions";

const initialState = {
  categories: [
    {_id: 'GD', title: 'Graphics&Design'},
    {_id: 'DM', title: 'Digital&Marketing'},
    {_id: 'VA', title: 'Video&Animation'},
    {_id: 'MA', title: 'Music&Audio'},
    {_id: 'PT', title: 'Programming&Tech'}
  ],
  status: [
    {_id: 'WA', title: 'waiting'},
    {_id: 'WO', title: 'working'},
    {_id: 'CL', title: 'closed'},
  ]
};

const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_SUCCESS:
      return {...state, categories: action.categories};
    default:
      return state;
  }
}

export default categoriesReducer