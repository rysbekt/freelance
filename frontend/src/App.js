import React from 'react';
import {Route, Switch} from "react-router-dom";
import {Container, CssBaseline} from "@material-ui/core";

import Categories from "./containers/Adds/Categories";
import AppToolBar from "./components/UI/AppToolBar/AppToolBar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewAdd from "./containers/NewAdd/NewAdd";
import SingleAdd from "./containers/SingleAdd/SingleAdd";
import {Helmet} from "react-helmet";
import ProgressAdd from "./containers/ProgressAdd/ProgressAdd";
import MyAdd from "./containers/MyAdd/MyAdd";


const App = () => {


  return (
    <>
      <Helmet titleTemplate="%s - Freelance"
              defaultTitle="Freelance"/>
      <CssBaseline/>
      <header><AppToolBar/></header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Categories}/>
            <Route path="/:category" exact component={Categories}/>
            <Route path="/adds/new" component={NewAdd}/>
            <Route path="/adds/my" component={MyAdd}/>
            <Route path="/adds/progress" component={ProgressAdd}/>
            <Route path="/add/:id" component={SingleAdd}/>
            <Route path="/user/register" exact component={Register}/>
            <Route path="/user/login" exact component={Login}/>
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;