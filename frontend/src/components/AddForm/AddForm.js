import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FileInput from "../UI/Form/FileInput";
import {MenuItem} from "@material-ui/core";
import {useSelector} from "react-redux";
import FormElement from "../UI/Form/FormElement";

const AddForm = ({onSubmit, categories}) => {
  const [state, setState] = useState({
    title: '',
    price: '',
    description: '',
    photo: '',
    category: ''
  });

  const error = useSelector(state => state.adds.addsError);

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    onSubmit(formData);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }


  return (
    <form onSubmit={submitFormHandler}>
      <Grid container direction="column" spacing={2}>
        <Grid item xs>
          <TextField
            required
            select
            variant="outlined"
            label="Category"
            name="category"
            value={state.category}
            onChange={inputChangeHandler}
          >
            <MenuItem><i>Select a category</i></MenuItem>
            {categories.map(category => (
              <MenuItem key={category._id} value={category._id}>
                {category.title}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <FormElement label={'Title'}
                     onChange={inputChangeHandler}
                     name={'title'}
                     value={state.title}
                     type={'text'}
                     error={getFieldError('title')}
        />
        <FormElement label={'Price'}
                     onChange={inputChangeHandler}
                     name={'price'}
                     value={state.price}
                     type={'number'}
                     error={getFieldError('price')}
        />
        <FormElement label={'Description'}
                     onChange={inputChangeHandler}
                     name={'description'}
                     value={state.description}
                     type={'text'}
                     error={getFieldError('description')}
        />
        <Grid item xs>
          <FileInput
            name='photo'
            label='Photo'
            required={true}
            onChange={fileChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <Button type="submit" color="primary" variant="contained">
            Create
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddForm;