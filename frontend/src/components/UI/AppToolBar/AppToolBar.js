import React from 'react';
import {AppBar, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";

const useStyles = makeStyles(theme => ({
  mainLink: {
    color: 'inherit',
    textDecoration: 'none',
    '&:hover': {
      color: 'inherit'
    }
  },
  staticToolbar: {
    marginBottom: theme.spacing(2)
  }
}));

const AppToolBar = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);
  const token = useSelector(state => state.users.token);


  return (
    <>
      <AppBar position="fixed">
        <Toolbar>
          <Grid container justify={"space-between"}>
            <Grid item>
              <Typography variant="h6">
                <Link to="/" className={classes.mainLink}>Callboard</Link>
              </Typography>
            </Grid>
            <Grid item>
              {token ? (<UserMenu user={user}/>) : (<AnonymousMenu/>)}
            </Grid>
          </Grid>

        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar}/>
    </>
  );
};

export default AppToolBar;