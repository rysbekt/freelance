import React from 'react';
import {Divider, Drawer, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import {makeStyles} from "@material-ui/core";
import CategoryIcon from '@material-ui/icons/Category';
import {historyPush} from "../../store/actions/historyActions";
import {useDispatch} from "react-redux";

const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: 'transparent'
  },
  toolbarFiller: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
}));

const SideBar = ({categories}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
      anchor="left"
    >
      <Divider/>
      <div className={classes.toolbarFiller}></div>
      <List>
        <ListItem button onClick={() => dispatch(historyPush('/'))}>
          <ListItemIcon>{<CategoryIcon/>}</ListItemIcon>
          <ListItemText primary={'All categories'}/>
        </ListItem>
        {categories.map(category => (
          <ListItem button onClick={() => dispatch(historyPush('/' + category._id))} key={category._id}>
            <ListItemIcon>{<CategoryIcon/>}</ListItemIcon>
            <ListItemText primary={category.title}/>
          </ListItem>
        ))}
      </List>
      <Divider/>
    </Drawer>
  );
};

export default SideBar;